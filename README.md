directory-meter
===============

Description
-----------

**directory-meter** is a command-line tool that lists directories by size, even if they aren't at the same tree level. It's designed to find the largest directory to archive when your hard drive is full.

Overview
--------

```
$ directory-meter show /
+-----------------------------+----------------+------------+-----------+
| Path                        |           Size |  Activity  |    Type   |
+-----------------------------+----------------+------------+-----------+
| /lost+found                 |           None | 2018-08-08 | DIRECTORY |
| /root                       |           None | 2020-04-10 | DIRECTORY |
| /cdrom                      |              0 | 2018-08-08 | DIRECTORY |
| /lib64                      |              0 | 2018-07-17 | DIRECTORY |
| /dev                        |              0 | 2020-05-19 | DIRECTORY |
| /media                      |              0 | 2018-08-08 | DIRECTORY |
| /home/john/Public           |              0 | 2018-08-08 | DIRECTORY |
| /mnt                        |              0 | 2018-07-17 | DIRECTORY |
| /home/john/.bash_logout     |            220 | 2018-08-08 |    FILE   |
| /home/john/.profile         |            807 | 2018-08-08 |    FILE   |
| /home/john/.bashrc          |          4,618 | 2020-02-13 |    FILE   |
| /home/john/.ssh             |         18,296 | 2019-07-28 | DIRECTORY |
| /home/john/tmp              |         24,569 | 2018-12-28 | DIRECTORY |
| /tmp                        |         34,719 | 2020-05-19 | DIRECTORY |
| /home/john/.bash_history    |         36,589 | 2020-05-19 |    FILE   |
| /home/john/Images           |         52,463 | 2019-08-11 | DIRECTORY |
| /run                        |         82,610 | 2020-05-19 | DIRECTORY |
| /home/john/bin              |      8,298,617 | 2020-01-06 | DIRECTORY |
| /etc                        |      8,806,485 | 2020-05-19 | DIRECTORY |
| /bin                        |     16,487,811 | 2020-05-19 | DIRECTORY |
| /sbin                       |     19,012,097 | 2020-05-19 | DIRECTORY |
| /home/john/.directory-meter |    120,285,223 | 2020-05-19 | DIRECTORY |
| /home/john/.var             |    123,397,397 | 2018-08-09 | DIRECTORY |
| /sys                        |    858,403,793 | 2020-05-19 | DIRECTORY |
| /home/john/.config          |  2,130,754,715 | 2020-05-11 | DIRECTORY |
| /swapfile                   |  2,147,483,648 | 2018-08-08 |    FILE   |
| /home/john/Documents        |  2,280,732,622 | 2019-08-24 | DIRECTORY |
| /boot                       |  2,303,169,912 | 2020-05-19 | DIRECTORY |
| /home/john/.cache           |  2,486,183,182 | 2020-05-11 | DIRECTORY |
| /opt                        |  3,320,899,985 | 2020-02-27 | DIRECTORY |
| /lib                        |  7,567,487,616 | 2018-08-20 | DIRECTORY |
| /usr                        |  9,199,745,398 | 2019-04-03 | DIRECTORY |
| /var                        | 17,614,343,915 | 2018-07-17 | DIRECTORY |
| /home/john/.local           | 39,336,508,866 | 2018-08-14 | DIRECTORY |
+-----------------------------+----------------+------------+-----------+
```

Installation
------------

```
$ pip3 install https://bitbucket.org/luc_phan/directory-meter/downloads/directory-meter-0.9.1.tar.gz
```

Configuration
-------------

```
$ directory-meter
[INFO] Creating config directory: /home/john/.directory-meter
[INFO] Creating config file: /home/john/.directory-meter/config.yml
Usage:
  directory-meter scan
  directory-meter show [<path>]
  directory-meter ls [<path>]
  directory-meter -h | --help
  directory-meter --version
$ vim ~/.directory-meter/config.yml
```

Setting `/` as target:

```yaml
---
targets:
  - /
...
```

Usage
-----

### Scanning targets

```
$ directory-meter scan
...
Total time: 53 minute(s)
Directories scanned: 1353605

$ ls ~/.directory-meter/
config.yml  data.sqlite3
```

### Showing largest directories

```
$ directory-meter show /
+------+---------------------+------------+-----------+
| Path |                Size |  Activity  |    Type   |
+------+---------------------+------------+-----------+
| /    | 140,827,020,212,269 | 2020-05-19 | DIRECTORY |
+------+---------------------+------------+-----------+
```

`/` is the largest directory, but it contains a lot of different things, I don't want to archive the whole bunch together.

### Adding a directory as branch

Setting `/` as branch:

```
$ vim ~/.directory-meter/config.yml
```

```yaml
---
targets:
  - /
branches:
  - /
...
```

```
$ directory-meter show /
+-------------+---------------------+------------+-----------+
| Path        |                Size |  Activity  |    Type   |
+-------------+---------------------+------------+-----------+
| /lost+found |                None | 2018-08-08 | DIRECTORY |
| /root       |                None | 2020-04-10 | DIRECTORY |
| /cdrom      |                   0 | 2018-08-08 | DIRECTORY |
| /lib64      |                   0 | 2018-07-17 | DIRECTORY |
| /dev        |                   0 | 2020-05-19 | DIRECTORY |
| /media      |                   0 | 2018-08-08 | DIRECTORY |
| /mnt        |                   0 | 2018-07-17 | DIRECTORY |
| /tmp        |              34,719 | 2020-05-19 | DIRECTORY |
| /run        |              82,610 | 2020-05-19 | DIRECTORY |
| /etc        |           8,806,485 | 2020-05-19 | DIRECTORY |
| /bin        |          16,487,811 | 2020-05-19 | DIRECTORY |
| /sbin       |          19,012,097 | 2020-05-19 | DIRECTORY |
| /sys        |         858,403,793 | 2020-05-19 | DIRECTORY |
| /swapfile   |       2,147,483,648 | 2018-08-08 |    FILE   |
| /boot       |       2,303,169,912 | 2020-05-19 | DIRECTORY |
| /opt        |       3,320,899,985 | 2020-02-27 | DIRECTORY |
| /lib        |       7,567,487,616 | 2018-08-20 | DIRECTORY |
| /usr        |       9,199,745,398 | 2019-04-03 | DIRECTORY |
| /var        |      17,614,343,915 | 2018-07-17 | DIRECTORY |
| /home       |      46,486,298,184 | 2018-08-08 | DIRECTORY |
| /proc       | 140,737,477,956,096 | 2020-05-19 | DIRECTORY |
+-------------+---------------------+------------+-----------+
```

`/proc` is the largest directory, but I don't want to archive that.

### Ignoring a directory

Ignoring `/proc`: 

```
$ vim ~/.directory-meter/config.yml
```

```yaml
---
targets:
  - /
branches:
  - /
ignore:
  - /proc
...
```

```
$ directory-meter show /
+-------------+----------------+------------+-----------+
| Path        |           Size |  Activity  |    Type   |
+-------------+----------------+------------+-----------+
| /lost+found |           None | 2018-08-08 | DIRECTORY |
| /root       |           None | 2020-04-10 | DIRECTORY |
| /cdrom      |              0 | 2018-08-08 | DIRECTORY |
| /lib64      |              0 | 2018-07-17 | DIRECTORY |
| /dev        |              0 | 2020-05-19 | DIRECTORY |
| /media      |              0 | 2018-08-08 | DIRECTORY |
| /mnt        |              0 | 2018-07-17 | DIRECTORY |
| /tmp        |         34,719 | 2020-05-19 | DIRECTORY |
| /run        |         82,610 | 2020-05-19 | DIRECTORY |
| /etc        |      8,806,485 | 2020-05-19 | DIRECTORY |
| /bin        |     16,487,811 | 2020-05-19 | DIRECTORY |
| /sbin       |     19,012,097 | 2020-05-19 | DIRECTORY |
| /sys        |    858,403,793 | 2020-05-19 | DIRECTORY |
| /swapfile   |  2,147,483,648 | 2018-08-08 |    FILE   |
| /boot       |  2,303,169,912 | 2020-05-19 | DIRECTORY |
| /opt        |  3,320,899,985 | 2020-02-27 | DIRECTORY |
| /lib        |  7,567,487,616 | 2018-08-20 | DIRECTORY |
| /usr        |  9,199,745,398 | 2019-04-03 | DIRECTORY |
| /var        | 17,614,343,915 | 2018-07-17 | DIRECTORY |
| /home       | 46,486,298,184 | 2018-08-08 | DIRECTORY |
+-------------+----------------+------------+-----------+
```

`/home` is the largest directory, but it contains a lot of different things, I don't want to archive the whole bunch together.

### Adding more branches

Setting `/home` as branch:

```
$ vim ~/.directory-meter/config.yml
```

```yaml
---
targets:
  - /
branches:
  - /
  - /home
ignore:
  - /proc
...
```

```
$ directory-meter show /
+-------------+----------------+------------+-----------+
| Path        |           Size |  Activity  |    Type   |
+-------------+----------------+------------+-----------+
| /lost+found |           None | 2018-08-08 | DIRECTORY |
| /root       |           None | 2020-04-10 | DIRECTORY |
| /cdrom      |              0 | 2018-08-08 | DIRECTORY |
| /lib64      |              0 | 2018-07-17 | DIRECTORY |
| /dev        |              0 | 2020-05-19 | DIRECTORY |
| /media      |              0 | 2018-08-08 | DIRECTORY |
| /mnt        |              0 | 2018-07-17 | DIRECTORY |
| /tmp        |         34,719 | 2020-05-19 | DIRECTORY |
| /run        |         82,610 | 2020-05-19 | DIRECTORY |
| /etc        |      8,806,485 | 2020-05-19 | DIRECTORY |
| /bin        |     16,487,811 | 2020-05-19 | DIRECTORY |
| /sbin       |     19,012,097 | 2020-05-19 | DIRECTORY |
| /sys        |    858,403,793 | 2020-05-19 | DIRECTORY |
| /swapfile   |  2,147,483,648 | 2018-08-08 |    FILE   |
| /boot       |  2,303,169,912 | 2020-05-19 | DIRECTORY |
| /opt        |  3,320,899,985 | 2020-02-27 | DIRECTORY |
| /lib        |  7,567,487,616 | 2018-08-20 | DIRECTORY |
| /usr        |  9,199,745,398 | 2019-04-03 | DIRECTORY |
| /var        | 17,614,343,915 | 2018-07-17 | DIRECTORY |
| /home/john  | 46,486,298,184 | 2020-05-19 | DIRECTORY |
+-------------+----------------+------------+-----------+
```

`/home/john` is the largest directory, but it contains a lot of different things, I don't want to archive the whole bunch together.

Setting `/home/john` as branch:

```
$ vim ~/.directory-meter/config.yml
```

```yaml
---
targets:
  - /
branches:
  - /
  - /home
  - /home/john
ignore:
  - /proc
...
```

```
$ directory-meter show /
+-----------------------------+----------------+------------+-----------+
| Path                        |           Size |  Activity  |    Type   |
+-----------------------------+----------------+------------+-----------+
| /lost+found                 |           None | 2018-08-08 | DIRECTORY |
| /root                       |           None | 2020-04-10 | DIRECTORY |
| /cdrom                      |              0 | 2018-08-08 | DIRECTORY |
| /lib64                      |              0 | 2018-07-17 | DIRECTORY |
| /dev                        |              0 | 2020-05-19 | DIRECTORY |
| /media                      |              0 | 2018-08-08 | DIRECTORY |
| /home/john/Public           |              0 | 2018-08-08 | DIRECTORY |
| /mnt                        |              0 | 2018-07-17 | DIRECTORY |
| /home/john/.bash_logout     |            220 | 2018-08-08 |    FILE   |
| /home/john/.profile         |            807 | 2018-08-08 |    FILE   |
| /home/john/.bashrc          |          4,618 | 2020-02-13 |    FILE   |
| /home/john/.ssh             |         18,296 | 2019-07-28 | DIRECTORY |
| /home/john/tmp              |         24,569 | 2018-12-28 | DIRECTORY |
| /tmp                        |         34,719 | 2020-05-19 | DIRECTORY |
| /home/john/.bash_history    |         36,589 | 2020-05-19 |    FILE   |
| /home/john/Images           |         52,463 | 2019-08-11 | DIRECTORY |
| /run                        |         82,610 | 2020-05-19 | DIRECTORY |
| /home/john/bin              |      8,298,617 | 2020-01-06 | DIRECTORY |
| /etc                        |      8,806,485 | 2020-05-19 | DIRECTORY |
| /bin                        |     16,487,811 | 2020-05-19 | DIRECTORY |
| /sbin                       |     19,012,097 | 2020-05-19 | DIRECTORY |
| /home/john/.directory-meter |    120,285,223 | 2020-05-19 | DIRECTORY |
| /home/john/.var             |    123,397,397 | 2018-08-09 | DIRECTORY |
| /sys                        |    858,403,793 | 2020-05-19 | DIRECTORY |
| /home/john/.config          |  2,130,754,715 | 2020-05-11 | DIRECTORY |
| /swapfile                   |  2,147,483,648 | 2018-08-08 |    FILE   |
| /home/john/Documents        |  2,280,732,622 | 2019-08-24 | DIRECTORY |
| /boot                       |  2,303,169,912 | 2020-05-19 | DIRECTORY |
| /home/john/.cache           |  2,486,183,182 | 2020-05-11 | DIRECTORY |
| /opt                        |  3,320,899,985 | 2020-02-27 | DIRECTORY |
| /lib                        |  7,567,487,616 | 2018-08-20 | DIRECTORY |
| /usr                        |  9,199,745,398 | 2019-04-03 | DIRECTORY |
| /var                        | 17,614,343,915 | 2018-07-17 | DIRECTORY |
| /home/john/.local           | 39,336,508,866 | 2018-08-14 | DIRECTORY |
+-----------------------------+----------------+------------+-----------+
```

### Listing contents at the same tree level

```
$ directory-meter ls /
+-----------------+---------------------+------------+-----------+
| Path            |                Size |  Activity  |    Type   |
+-----------------+---------------------+------------+-----------+
| /lost+found     |                None | 2018-08-08 | DIRECTORY |
| /root           |                None | 2020-04-10 | DIRECTORY |
| /cdrom          |                   0 | 2018-08-08 | DIRECTORY |
| /lib64          |                   0 | 2018-07-17 | DIRECTORY |
| /dev            |                   0 | 2020-05-20 | DIRECTORY |
| /media          |                   0 | 2020-05-19 | DIRECTORY |
| /mnt            |                   0 | 2018-07-17 | DIRECTORY |
| /tmp            |              38,341 | 2020-05-20 | DIRECTORY |
| /run            |              82,956 | 2020-05-20 | DIRECTORY |
| /vmlinuz.old    |           8,380,056 | 2020-04-22 |    FILE   |
| /vmlinuz        |           8,380,064 | 2020-05-11 |    FILE   |
| /etc            |           8,806,184 | 2020-05-20 | DIRECTORY |
| /bin            |          16,487,811 | 2020-05-19 | DIRECTORY |
| /sbin           |          19,012,097 | 2020-05-20 | DIRECTORY |
| /initrd.img.old |          61,229,377 | 2020-05-13 |    FILE   |
| /initrd.img     |          61,264,434 | 2020-05-20 |    FILE   |
| /sys            |         859,771,893 | 2020-05-20 | DIRECTORY |
| /swapfile       |       2,147,483,648 | 2018-08-08 |    FILE   |
| /boot           |       2,303,197,295 | 2020-05-20 | DIRECTORY |
| /opt            |       3,321,305,794 | 2020-05-20 | DIRECTORY |
| /lib            |       7,567,479,424 | 2020-05-20 | DIRECTORY |
| /usr            |       9,199,744,113 | 2020-05-20 | DIRECTORY |
| /var            |      17,728,528,661 | 2020-05-20 | DIRECTORY |
| /home           |      46,435,832,572 | 2020-05-20 | DIRECTORY |
| /proc           | 140,737,477,956,096 | 2020-05-20 | DIRECTORY |
+-----------------+---------------------+------------+-----------+
```

### Changing logging configuration

If you know about **Python dictConfig**, you can customize the logging configuration :

```
$ vim ~/.directory-meter/config.yml
```

```yaml
---
targets:
  - /
branches:
  - /
  - /home
  - /home/john
ignore:
  - /proc
logging:
  version: 1
  disable_existing_loggers: True
  formatters:
    standard:
      # format: "[%(asctime)s][%(levelname)s] %(message)s"
      format: "[%(levelname)s] %(message)s"
  handlers:
    console:
      level: DEBUG
      class: logging.StreamHandler
      formatter: standard
  loggers:
    directorymeter:
      handlers: [console]
#      level: DEBUG
      level: INFO
...
```

### Search node by name

```
$ directory-meter search abc
```

### Sort by activity

```
$ directory-meter show --sort activity  /
$ directory-meter ls -s activity /
$ directory-meter search -s activity abc
```

### Reverse sort

```
$ directory-meter show --sort activity --reverse /
$ directory-meter ls -s activity -r /
$ directory-meter search -s activity -r abc
```

Development
-----------

### Test

```
$ pipenv shell
$ python setup.py develop
$ directory-meter
```

### Build

```
$ pipenv shell
$ python setup.py sdist
$ ls dist
```

### Argument parsing

- ~~argparse~~
- docopt
- ~~click~~

### TODO

- ~~Perf: Remove generator from show command~~
- ~~Refactor: Use NodeWalker in Scan~~
- ~~Refactor: NodeWalker inheritance~~
- ~~Feat: Summary (total time, total directories)~~
- ~~Fix: Locate freeze in code (PyCharm terminal bug?)~~
- ~~Doc~~
- ~~Feat: `directory-meter search string`~~
- ~~Refactor: Add name to node model to allow searches~~
- ~~Refactor: Store files to allow searches ?~~
- ~~Feat: Sort by activity~~
- ~~Feat: Relative path output~~
- ~~Feat: Store sort field in config file~~

import logging
import datetime
from pathlib import Path
from .database import Database, Node

_logger = logging.getLogger(__name__)


def _create_node(path):
    size = None
    activity = None
    type_ = None

    try:
        stat = path.stat()
        activity = datetime.datetime.fromtimestamp(stat.st_mtime)
        if path.is_file():
            type_ = Node.TYPE_FILE
            size = stat.st_size
        elif path.is_dir():
            type_ = Node.TYPE_DIRECTORY
            size = None
        else:
            _logger.warning("Unknown node type: %s", str(path))
    except OSError as e:
        _logger.warning(e)

    return Node(path=str(path), name=path.name, size=size, activity=activity, type=type_)


class NodeWalker:
    def __init__(self, ignore):
        self._ignore = ignore

    def walk(self, path):
        if self._is_ignored(path):
            _logger.debug("Ignored: %s", path)
            return

        _logger.debug("Scanning: %s", path)

        try:
            if path.is_symlink():
                _logger.warning("Link skipped: %s", path)
                return
        except OSError as e:
            _logger.warning(e)
            return

        try:
            str(path).encode('utf-8')
        except UnicodeEncodeError:
            _logger.warning("Non-unicode name: %s", path)
            return

        node = _create_node(path)
        self._process_node(node)
        return node

    def _is_ignored(self, path):
        return any(Path(path) == ignored_path for ignored_path in self._ignore)

    def _process_node(self, node):
        pass


class Scan(NodeWalker):

    def __init__(self, database_file, targets, ignore):
        super().__init__(ignore)
        self._database_file = database_file
        self._targets = targets
        self._session = None

    def run(self):
        if self._database_file.exists():
            _logger.info("Removing: %s", self._database_file)
            self._database_file.unlink()

        _logger.info("Creating: %s", self._database_file)
        database = Database(self._database_file)
        self._session = database.new_session()

        for target in self._targets:
            _logger.info("Scanning: %s", target)
            self.walk(target)

        self._session.commit()
        row_count = self._session.query(Node).count()
        self._session.close()
        database.dispose()

        return row_count

    def _process_node(self, node):
        if node.is_file():
            self._session.add(node)
        elif node.is_dir():
            size = 0
            activity = node.activity

            try:
                for path_ in Path(node.path).iterdir():
                    node_ = self.walk(path_)
                    if node_:
                        if node_.size is not None:
                            size += node_.size
                        if node_.activity is not None and activity < node_.activity:
                            activity = node_.activity
            except (PermissionError, FileNotFoundError, OSError) as e:
                _logger.warning(e)
                return

            node.size = size
            node.activity = activity
            self._session.add(node)
        else:
            _logger.warning("Unknown node type: " + node.path)


class NodeLister(NodeWalker):
    def __init__(self, database_file, branches, ignore):
        super().__init__(ignore)
        self._database_file = database_file
        self._branches = branches
        self._session = None
        self._node_list = None

    def list(self, path):
        database = Database(self._database_file)
        self._session = database.new_session()
        self._node_list = list()

        self.walk(path)
        database.dispose()
        return self._node_list

    def _process_node(self, node):
        if node.is_file():
            self._node_list.append(node)
        elif node.is_dir():
            if self._is_branch(node):
                for path_ in Path(node.path).iterdir():
                    self.walk(path_)
                return

            nodes = self._session.query(Node).filter_by(path=node.path).all()
            if len(nodes) == 1:
                self._node_list.append(nodes[0])
            elif len(nodes) == 0:
                self._node_list.append(_create_node(Path(node.path)))
            else:
                raise RuntimeError("Duplicate nodes for path: " + node.path)
        else:
            _logger.warning("Unknown node type: " + node.path)

    def _is_branch(self, node):
        return any(Path(node.path) == path for path in self._branches)


class NodeLS(NodeWalker):
    def __init__(self, database_file, branches, ignore):
        super().__init__(ignore)
        self._database_file = database_file
        self._branches = branches
        self._session = None
        self._node_list = None

    def list(self, path):
        database = Database(self._database_file)
        self._session = database.new_session()
        self._node_list = list()

        self.walk(path)
        database.dispose()

        return self._node_list

    def _process_node(self, node):
        if node.is_file():
            self._node_list.append(node)
        elif node.is_dir():
            for path_ in Path(node.path).iterdir():

                try:
                    str(path_).encode('utf-8')
                except UnicodeEncodeError:
                    _logger.warning("Non-unicode name: %s", path_)
                    continue

                nodes = self._session.query(Node).filter_by(path=str(path_)).all()
                if len(nodes) == 1:
                    self._node_list.append(nodes[0])
                elif len(nodes) == 0:
                    self._node_list.append(_create_node(Path(path_)))
                else:
                    raise RuntimeError("Duplicate nodes for path: " + node.path)
        else:
            _logger.warning("Unknown node type: " + node.path)

    def _is_branch(self, node):
        return any(Path(node.path) == path for path in self._branches)


class Search:
    def __init__(self, database_file):
        self._database_file = database_file

    def search(self, string):
        database = Database(self._database_file)
        session = database.new_session()

        nodes = session.query(Node).filter(Node.name.contains(string)).all()
        database.dispose()

        return nodes

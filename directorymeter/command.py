"""
List nodes by size.

Usage:
  directory-meter scan
  directory-meter show [-s=COLUMN] [-r] [<path>]
  directory-meter ls [-s=COLUMN] [-r] [<path>]
  directory-meter search [-s=COLUMN] [-r] <string>
  directory-meter -h | --help
  directory-meter --version

Options:
  -s=COLUMN, --sort=COLUMN  Sort column.
  -r, --reverse  Reverse sort order [default: False].
"""

import logging.config
from docopt import docopt
from pathlib import Path
import shutil
import yaml
from prettytable import PrettyTable
import time
from datetime import datetime

import directorymeter
from .core import Scan, NodeLister, NodeLS, Search

_LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            # 'format': "[%(asctime)s][%(levelname)s] %(message)s"
            'format': "[%(levelname)s] %(message)s"
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            # 'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        }
    },
    'loggers': {
        'directorymeter': {
            'handlers': ['console'],
            'level': 'DEBUG'
        }
    }
}
logging.config.dictConfig(_LOGGING)
_logger = logging.getLogger(__name__)


def main():
    command = Command()
    command.run()


class Command:
    caption = 'directory-meter ' + directorymeter.__version__
    _config_directory = Path.home() / '.directory-meter'
    _database_file = _config_directory / 'data.sqlite3'
    _new_database_file = _config_directory / 'data.new.sqlite3'

    def __init__(self):
        self._targets = list()
        self._branches = list()
        self._ignore = list()
        self._sort = 'size'

        config = self._init_config()

        if 'logging' in config:
            logging.config.dictConfig(config['logging'])
        if 'targets' in config:
            self._targets = [Path(target).resolve() for target in config['targets']]
        if 'branches' in config:
            self._branches = [Path(branch).resolve() for branch in config['branches']]
        if 'ignore' in config:
            self._ignore = [Path(ignore) for ignore in config['ignore']]
        if 'sort' in config:
            self._sort = config['sort']

    def _init_config(self):
        config = Config(self._config_directory)
        config.create()
        return config.load()

    def run(self):
        arguments = docopt(__doc__, version=Command.caption)

        _logger.debug("Arguments: %s", arguments)

        if arguments['scan']:
            self._scan()
        elif arguments['show']:
            sort = arguments['--sort'] or self._sort
            self._check_sort(sort)
            self._show(arguments['<path>'], sort, arguments['--reverse'])
        elif arguments['ls']:
            sort = arguments['--sort'] or self._sort
            self._check_sort(sort)
            self._ls(arguments['<path>'], sort, arguments['--reverse'])
        elif arguments['search']:
            sort = arguments['--sort'] or self._sort
            self._check_sort(sort)
            self._search(arguments['<string>'], sort, arguments['--reverse'])

    def _check_sort(self, sort):
        if sort not in ('size', 'activity'):
            raise ValueError("Invalid sort column: " + str(sort))

    def _scan(self):
        start = time.time()

        scan = Scan(self._new_database_file, self._targets, self._ignore)
        directory_count = scan.run()
        self._new_database_file.replace(self._database_file)

        end = time.time()
        delta = end - start
        minutes = int(delta / 60)
        print("Total time: {} minute(s)".format(minutes))
        print("Directories scanned:", directory_count)

    def _show(self, path_string, sort, reverse):
        if path_string:
            path = Path(path_string).resolve()
        else:
            path = Path('.').resolve()

        lister = NodeLister(self._database_file, self._branches, self._ignore)
        nodes = lister.list(path)
        self._display_nodes(nodes, sort, reverse)

    def _ls(self, path_string, sort, reverse):
        if path_string:
            path = Path(path_string).resolve()
        else:
            path = Path('.').resolve()

        lister = NodeLS(self._database_file, self._branches, self._ignore)
        nodes = lister.list(path)
        self._display_nodes(nodes, sort, reverse)

    def _search(self, string, sort, reverse):
        searcher = Search(self._database_file)
        nodes = searcher.search(string)
        self._display_nodes(nodes, sort, reverse)

    @staticmethod
    def _display_nodes(nodes, sort, reverse):
        t = PrettyTable(['Path', 'Size', 'Activity', 'Type'])
        t.align['Path'] = 'l'
        t.align['Size'] = 'r'

        cwd = Path.cwd()

        if sort == 'size':
            default_reverse = False
            direction = default_reverse != reverse
            nodes.sort(key=lambda n: -1 if n.size is None else n.size, reverse=direction)
        elif sort == 'activity':
            default_reverse = True
            direction = default_reverse != reverse
            nodes.sort(key=lambda n: n.activity or datetime.min, reverse=direction)
        else:
            raise ValueError("Invalid sort column: " + sort)

        for node in nodes:
            type_ = node.type
            if node.is_file():
                type_ = 'FILE'
            elif node.is_dir():
                type_ = 'DIRECTORY'

            if node.size is None:
                size = None
            else:
                size = '{:,}'.format(node.size)

            if node.activity is None:
                activity = None
            else:
                activity = node.activity.strftime('%Y-%m-%d')

            try:
                path = Path(node.path).relative_to(cwd)
            except ValueError:
                path = node.path

            t.add_row([path, size, activity, type_])

        print(t)


class Config:
    _dist_file = Path(__file__).parent / 'data' / 'config.dist.yml'

    def __init__(self, directory):
        self._directory = directory
        self._file = directory / 'config.yml'

    def create(self):
        if not self._directory.is_dir():
            _logger.info("Creating config directory: %s", self._directory)
            self._directory.mkdir()
        if not self._file.is_file():
            _logger.info("Creating config file: %s", self._file)
            shutil.copyfile(self._dist_file, self._file)

    def load(self):
        with self._file.open(encoding='utf-8') as f:
            return yaml.safe_load(f)

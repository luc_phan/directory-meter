import logging
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Sequence, UniqueConstraint, Integer, Unicode, BigInteger, DateTime

_logger = logging.getLogger(__name__)
Base = declarative_base()


class Database:
    def __init__(self, database_file):
        self._database_file = database_file
        self._database_string = 'sqlite:///' + str(database_file)
        self._engine = engine = create_engine(self._database_string)
        Base.metadata.create_all(engine)
        self._session_class = sessionmaker(bind=engine)

    def new_session(self):
        return Session(self._session_class())

    def dispose(self):
        self._engine.dispose()


class Session:
    _MAX_TRANSACTION = 1000

    def __init__(self, sqlalchemy_session):
        self._sqlalchemy_session = sqlalchemy_session
        self._transactions = 0

    def commit(self):
        _logger.debug("Committing...")
        self._sqlalchemy_session.commit()
        self._transactions = 0

    def add(self, object_):
        self._sqlalchemy_session.add(object_)
        self._transactions += 1
        if self._transactions > self._MAX_TRANSACTION:
            self.commit()

    def delete(self, object_):
        self._sqlalchemy_session.delete(object_)

    def close(self):
        self._sqlalchemy_session.close()

    def query(self, model):
        return self._sqlalchemy_session.query(model)


class Node(Base):

    TYPE_FILE = 1
    TYPE_DIRECTORY = 2

    __tablename__ = 'nodes'
    id = Column(Integer, Sequence('infos_id_seq'), primary_key=True)
    path = Column(Unicode(256))
    name = Column(Unicode(256))
    size = Column(BigInteger)
    activity = Column(DateTime)
    type = Column(Integer)
    __table_args__ = (UniqueConstraint('path'),)

    def __repr__(self):
        return "<Node(path='{}', name='{}', size={}, activity={}, type={})>".format(self.path, self.name, self.size, self.activity, self.type)

    def is_file(self):
        return self.type == Node.TYPE_FILE

    def is_dir(self):
        return self.type == Node.TYPE_DIRECTORY

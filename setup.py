from setuptools import setup
import directorymeter

setup(
    name='directory-meter',
    version=directorymeter.__version__,
    packages=['directorymeter'],
    url='',
    license='',
    author='Luc PHAN',
    author_email='',
    description='',
    entry_points={'console_scripts': ['directory-meter = directorymeter.command:main']},
    install_requires=['docopt', 'pyyaml', 'sqlalchemy', 'PrettyTable'],
    include_package_data=True
)
